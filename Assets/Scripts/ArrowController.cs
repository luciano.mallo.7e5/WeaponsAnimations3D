using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{

    void Start()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.AddForce((transform.forward) * 25f, ForceMode.Impulse);
    }


}
