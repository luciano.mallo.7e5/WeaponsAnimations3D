using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcController : MonoBehaviour
{
    private Animator animator;
    public GameObject arrowPrefab;
    private bool isPlaying;
    private void Awake()
    {
        animator = GetComponent<Animator>();

    }
    private void Update()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("FinishShoot") && !isPlaying)
        {
            isPlaying = true;
            Instantiate(arrowPrefab, transform);
            // StartCoroutine(WaitToInstantiateArrow(animator.GetCurrentAnimatorStateInfo(0).normalizedTime));

        }
        else
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("FinishShoot"))
                isPlaying = false;
        }
    }
    IEnumerator WaitToInstantiateArrow(float length)
    {

        yield return new WaitForSeconds(length);
        Instantiate(arrowPrefab, transform);
        isPlaying = false;
    }
}
